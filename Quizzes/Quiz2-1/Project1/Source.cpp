#include <iostream>
#include <string>


//1. Computing the area & circumference given a radius


using namespace std;

int main()
{
// input radius

	int radius ;

	cout << "Input the radius to solve the area and circumference of a circle : " << endl ;
	cin >> radius ;

	// compute area & circumference

	float pi = 3.14 ;
	float area = pi * (radius ^ 2) ;
	float circumference = 2 * pi * radius ;

	// output

	cout << "Area : " << area << endl ;
	cout << "Circumference : " << circumference << endl ;

	system("pause");
	return 0;
}