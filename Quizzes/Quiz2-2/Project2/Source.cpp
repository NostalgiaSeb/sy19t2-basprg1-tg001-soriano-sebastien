#include <iostream>
#include <string>


//2. Computing the sum & average of 5 numbers


using namespace std;

int main()
{
	// input numbers

	float n1 ;
	float n2 ;
	float n3 ;
	float n4 ;
	float n5 ;

	cout << "Input ANY FIVE numbers to compute for the Sum & Average : " << endl ;
	cout << "Input first number : " << endl;
	cin >> n1 ;
	cout << "Input second number : " << endl;
	cin >> n2;
	cout << "Input third number : " << endl;
	cin >> n3;
	cout << "Input fourth number : " << endl;
	cin >> n4;
	cout << "Input fifth number : " << endl;
	cin >> n5;

	// compute numbers

	float sum = n1 + n2 + n3 + n4 + n5 ;
	float avg = sum / 2 ;

	// output

	cout << "Sum : " << sum << endl ;
	cout << "Average : " << avg << endl;


	system("pause");
	return 0;
}